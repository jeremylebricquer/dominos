from dominos import Domino

# Test constructeur
mon_domino = Domino(4,6)
mon_domino.__repr__()
assert mon_domino.A in range(0, 7)
assert mon_domino.B in range(0, 7)
mon_domino = Domino(0, 5)
assert mon_domino.A == 0
assert mon_domino.B == 5
mon_autre_domino = Domino(1, 4)
assert mon_autre_domino.__repr__() == '[1:4]'
assert mon_domino.__repr__() == '[ :5]'
print('domino_1',mon_domino,'domino_2',mon_autre_domino)
print('------------ Test constructeur OK ---------------')


# Doit lever une exception
mon_faux_domino = Domino(1, 7)
mon_faux_domino = Domino(-1, 0)
mon_faux_domino = Domino(7, 7 )
print('------------ Test range OK ---------------')


# Test inverse()
mon_domino = Domino(0, 5)
print(mon_domino)
mon_domino.inverse()
assert mon_domino.A == 5
assert mon_domino.B == 0
print(mon_domino)
print('------------ Test score() OK ---------------')


# Test score()
mon_autre_domino = Domino(1, 4)
print(mon_autre_domino)
assert mon_autre_domino.score() == 5
print('------------ Test score() OK ---------------')


# Test est_compatible()
mon_domino = Domino(0, 5)
assert mon_domino.est_compatible(4) == False
mon_autre_domino = Domino(1, 4)
assert mon_autre_domino.est_compatible(4) == True
assert mon_autre_domino.est_compatible(5) == False
assert mon_autre_domino.est_compatible(0) == False
print('------------ Test est_compatible() OK ---------------')