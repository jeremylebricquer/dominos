import random
from dominos import Domino

class Main:

    def __init__(self,nb_joueur = 2):
      self.nb_joueur = nb_joueur

    def players(self):
        self.nb_joueur = int(input('Nombre de joueurs: '))
        return self.nb_joueur

    def generate_domino():
        '''
        generate list dominoes
        '''
        list_domino = []
        for i in range(0, 7):
            for j in range(i, 7):
                list_domino.append(Domino(i, j))
        return list_domino

    def diff(a, b):
        '''
        return elements that exist in a and not exist in b
        '''
        return [aa for aa in a if aa not in b]

    def generate_hand(self):
        '''
            generate unique hand for each player
        '''
        list_domino = Main.generate_domino()

        if self.nb_joueur == 2:
            player_one_cards= random.sample(list_domino, k=7)
            reste = Main.diff(list_domino,player_one_cards)
            player_two_cards = random.sample(reste, 7)
            reste = Main.diff(reste, player_two_cards)
            return [player_one_cards,player_two_cards,reste] 
        #elif 3 <= self.nb_joueur <= 4:
            #player_one_cards = random.sample(list_domino, k=6)
            #reste = Game.diff(list_domino,player_one_cards)
            #player_two_cards = random.sample(reste, 7)
            #reste = Game.diff(reste, player_two_cards)
            #print(f'3|4j: {self.nb_joueur} la main {hand}')
            #return hand
        #elif 5 <= self.nb_joueur <= 6:
            #hand = random.sample(list_domino, k=4)
            #print(f'5|6j: {self.nb_joueur} la main {hand}')
            #return hand
        
    def affiche_hand(self):
        '''
            Display hand and rest
        '''
        liste =  Main.generate_hand(self)
        for i,j in enumerate(liste[:2]):
            print(f'Main du joueur{i} : ',j)
        print(f'reste {liste[-1]}')

    def pioche(self):
        reste= Main.generate_hand(self)[-1]

        pioche_dominos_talons = random.choice(reste)
        reste.append(pioche_dominos_talons)
        reste.remove(pioche_dominos_talons)
        return pioche_dominos_talons , reste





game = Main()
#game.players()
#game.generate_hand()
#game.affiche_hand()
game.pioche()



