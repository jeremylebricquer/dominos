
from dominos import Domino
from Dominos.chaine import Chaine
from class.main import Main
from Dominos.talon import Talon


class Game:

    def __init__(self):
        input("Appuyez sur entrée pour commencer la partie")
        board = Chaine()
        talon = Talon()
        first_domino = talon.pioche()
        print(talon)
        board.pose_premier(first_domino)
        # self.show_board(board)

    def check_score(self, nb_joueur):
        main = Main.generate_hand(self, nb_joueur)
        list1, list2 = [], []
        for val1 in main[:1]:
            for domino in val1:
                list1.append(Domino.score(domino))
                max_value_1 = max(list1)
                max_index_1 = list1.index(max_value_1)
        for val2 in main[1:2]:
            for domino in val2:
                list2.append(Domino.score(domino))
                max_value_2 = max(list2)
                max_index_2 = list2.index(max_value_2)

        if max_value_1 > max_value_2:
            print(
                f'Le joueur 1 a un score de {max_value_1} et commence avec le domino suivant {val1[max_index_1]}')
            return val1[max_index_1]
        else:
            print(
                f'Le joueur 2 a un score de {max_value_2} et commence avec le domino suivant {val2[max_index_2]}')
            return val2[max_index_2]

    def start(self, nb_joueur):
        premier = Game.check_score(self, nb_joueur)
        return premier

    def show_board(board):
        print(str(board))

    def play_round(board, domino_extremites, domino):
        domino_reverse = Domino.inverse()

        if domino_extremites[0].est_compatible(domino.faceB):
            if domino_extremites[0].faceA == domino.faceB:
                board.ajoute_au_cote_gauche(domino)
            else:
                board.ajoute_au_cote_gauche(domino_reverse)
        elif domino_extremites[1].est_compatible(domino.faceA):
            if domino_extremites[1].faceB == domino.faceA:
                board.ajoute_au_cote_droit(domino)
            else:
                board.ajoute_au_cote_droit(domino_reverse)
        else:
            print("Impossible de poser le domino: " + str(domino))

        return None

    def win():
        pass


game = Game()
