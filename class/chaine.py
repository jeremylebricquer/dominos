class Chaine:

    def __init__(self):
        self.c = []

    @property
    def A(self):
        return self.c[0].A

    @property
    def B(self):
        return self.c[-1].B

    def extremites(self):
        return self.c[0], self.c[-1]

    def pose_premier(self, domino):
        self.c.append(domino)


    def ajoute_au_cote_gauche(self, domino):
        self.c.insert(0, domino)

    def ajoute_au_cote_droit(self, domino):
        self.c.append(domino)

    def __repr__(self):
        return "".join([str(domino) for domino in self.c])
