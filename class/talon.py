import random
from dominos import Domino


class Talon:
    """
    Talon (pioche) du jeu de domino
    """

    def __init__(self, t=None):

        # Liste defini
        if t == None:
            dominos = [0, 1, 2, 3, 4, 5, 6]
            dominos2 = [0, 1, 2, 3, 4, 5, 6]
            liste_dominos = []
            liste_dominos_talons = []

            for x in dominos:
                for y in dominos2:

                    d1 = Domino(x, y)
                    liste_dominos.append(Domino(x, y))
                dominos2.pop(0)

            # liste de 10

            for x in range(10):

                dominos_talons = random.choice(liste_dominos)
                liste_dominos_talons.append(dominos_talons)
                liste_dominos.remove(dominos_talons)

            self.t = liste_dominos_talons

        else:
            self.t = t
            self.vide = False

    def pioche(self):

        if len(self.t) == 1:
            self.t = []
        elif len(self.t) == 0:
            return None
        else:
            domino_pioche = []
            domino_random = random.choice(self.t)
            domino_pioche.append(domino_random)
            self.t = domino_pioche

    def __repr__(self):
        return f"[{self.t}:{self.t}]"
