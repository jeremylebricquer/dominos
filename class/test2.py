from dominos import Domino
import random
# Jeu de dominos à un seul joueur humain
from dominos import *
import os

# Fonction utilitaire


def efface_console():
    os.system('cls' if os.name == 'nt' else 'clear')


class Domino:

    def __init__(self, A=None, B=None):

        if A is None:
            A = randint(0, 6)
        if B is None:
            B = randint(0, 6)
        self.A = A
        self.B = B
        if (self.A not in range(7)) or (self.B not in range(7)):
            raise AttributeError('Valeur incorrecte pour le domino')

    def score(self):
        """
        Calcul des points du domino
        """
        return self.A + self.B

    def inverse(self):
        """
        Inverse la position du domino
        """
        self.A, self.B = self.B, self.A
        return self

    def __repr__(self):
        """
        Renvoie une rpésentation de l'objet
        """
        if self.A == 0:
            return f"[ :{self.B}]"
        elif self.B == 0:
            return f"[{self.A}:]"
        elif self.A == 0 and self.B == 0:
            return f"[ : ]"
        else:
            return f"[{self.A}:{self.B}]"

    def est_compatible(self, valeur):
        """
        Teste si le domino est compatible avec une valeur passée en paramètre
        c'est-à-dire s'il peut être placée à côté de cette valeur
        """
        if valeur == 0:
            return True
        if self.A == valeur or self.B == valeur or self.A == 0 or self.B == 0:
            return True
        else:
            return False

    def peut_etre_place_a_gauche_de(self, valeur):
        if valeur == 0:
            return True
        if self.B == valeur or self.B == 0:
            return True
        else:
            return False

    def peut_etre_place_a_droite_de(self, valeur):
        if valeur == 0:
            return True
        if self.A == valeur or self.A == 0:
            return True
        else:
            return False


class Talon:
    """
    Talon (pioche) du jeu de domino
    """

    def __init__(self, t=None):

        # Liste defini
        if t == None:
            dominos = [0, 1, 2, 3, 4, 5, 6]
            dominos2 = [0, 1, 2, 3, 4, 5, 6]
            liste_dominos = []
            liste_dominos_talons = []

            for x in dominos:
                for y in dominos2:

                    d1 = Domino(x, y)
                    liste_dominos.append(Domino(x, y))
                dominos2.pop(0)

            # liste de 10

            for x in range(10):

                dominos_talons = random.choice(liste_dominos)
                liste_dominos_talons.append(dominos_talons)
                liste_dominos.remove(dominos_talons)

            self.t = liste_dominos_talons

        else:
            self.t = t
            self.vide = False

    def pioche(self):

        if len(self.t) == 1:
            self.t = []
        elif len(self.t) == 0:
            return None
        else:
            domino_pioche = []
            domino_random = random.choice(self.t)
            domino_pioche.append(domino_random)
            self.t = domino_pioche

    def __repr__(self):
        return f"[{self.t}:{self.t}]"


class Chaine:

    def __init__(self):
        self.c = []

    @property
    def A(self):
        return self.c[0].A

    @property
    def B(self):
        return self.c[-1].B

    def extremites(self):
        return self.c[0], self.c[-1]

    def pose_premier(self, domino):
        self.c.append(domino)

    def ajoute_au_cote_gauche(self, domino):
        self.c.insert(0, domino)

    def ajoute_au_cote_droit(self, domino):
        self.c.append(domino)

    def __repr__(self):
        return "".join([str(domino) for domino in self.c])


print("--------------------------------------------------------")
efface_console()
mes_domino = Talon()
domino = Talon()
domino.pioche()
print(domino.t)
print(mes_domino.t)

chaine = Chaine()
chaine.pose_premier(domino.t[0])


def jeu():

    while mes_domino.t != []:
        efface_console()
        print(chaine.c)
        indices = ""
        print("vos dominos : "+str(mes_domino.t))
        for i in range(len(mes_domino.t)):
            indices += f'  {i}    '
        print("vos dominos : ", indices)
        reponse_G_D = input('Choisissez entre gauche ou droite : ')
        if(reponse_G_D == "gauche"):
            reponse_D = input('Choisissez votre domino: ')
            reponse_I = input('Voulez vous l inverser (oui/non): ')
            if(reponse_I == "oui"):

                mes_domino.t[int(reponse_D)].inverse()

            chaine.ajoute_au_cote_gauche(mes_domino.t[int(reponse_D)])
            del mes_domino.t[int(reponse_D)]

        elif(reponse_G_D == "droite"):
            reponse_D = input('Choisissez votre domino: ')
            reponse_I = input('Voulez vous l inverser (oui/non): ')
            if(reponse_I == "oui"):

                mes_domino.t[int(reponse_D)].inverse()

            chaine.ajoute_au_cote_droit(mes_domino.t[int(reponse_D)])
            del mes_domino.t[int(reponse_D)]


jeu()
