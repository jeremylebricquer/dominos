from random import randint, sample


class Domino:

    def __init__(self, A=None, B=None):

        if A is None:
            A = randint(0, 6)
        if B is None:
            B = randint(0, 6)
        self.A = A
        self.B = B
        if (self.A not in range(7)) or (self.B not in range(7)):
            raise AttributeError('Valeur incorrecte pour le domino')

    def score(self):
        """
        Calcul des points du domino
        """
        return self.A + self.B

    def inverse(self):
        """
        Inverse la position du domino
        """
        self.A, self.B = self.B, self.A
        return self

    def __repr__(self):
        """
        Renvoie une rpésentation de l'objet
        """
        if self.A == 0:
            return f"[ :{self.B}]"
        elif self.B == 0:
            return f"[{self.A}:]"
        elif self.A == 0 and self.B == 0:
            return f"[ : ]"
        else:
            return f"[{self.A}:{self.B}]"

    def est_compatible(self, valeur):
        """
        Teste si le domino est compatible avec une valeur passée en paramètre
        c'est-à-dire s'il peut être placée à côté de cette valeur
        """
        if valeur == 0:
            return True
        if self.A == valeur or self.B == valeur or self.A == 0 or self.B == 0:
            return True
        else:
            return False
