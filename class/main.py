from random import randint, sample
from dominos import Domino


class Main:
    """
    Gestion de la main du joueur
    m : liste de dominos
    """

    def __init__(self, m=None):
        self.m = m

        if m == None:
            liste_dominos = []

            for i in range(0, 7):
                for j in range(i, 7):
                    liste_dominos.append(Domino(i, j))

            dominos_main = sample(liste_dominos, k=5)
            self.m = dominos_main

        else:
            self.m = m

    def plus_haute_valeur(self):
        # Plus haute valeur dans la main du joueur
        score = None
        position = None

        for key, val in enumerate(self.m):
            score = Domino.score(val)
        position = key
        return (score, position)

    def domino_de_plus_haute_valeur(self):
        # Extrait et renvoie le domino de plus haute valeur
        _, position_max = self.plus_haute_valeur()
        domino_choisi = None

        if position_max is not None:
            domino_choisi = self.m.pop(position_max)
        return domino_choisi

    def selectionne(self, indice):
        # Selectionne le domino tout en le laissant dans la main
        domino_choisi = self.m[indice]
        return domino_choisi

    def retire(self, indice):
        # Retire le domino de la main
        domino_choisi = self.m.pop(indice)
        return domino_choisi

    def est_compatible(self, valeur):
        # Renvoie vrai s'il y a un domino dans la main compatible avec la valeur passée en paramètre
        compatible = False

        for i in self.m:
            compatible = Domino.est_compatible(i, valeur)

        return compatible

    def ajoute(self, domino):
        self.m.append(domino)

    def __repr__(self):
        representation = ""
        if self.m:
            for domino in self.m:
                representation += repr(domino)
        return representation

    @classmethod
    def de_taille(cls, n):
        main = cls([Domino() for _ in range(n)])
        return main
