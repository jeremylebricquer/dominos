from random import randint, sample, choice


class Domino:

    def __init__(self, A=None, B=None):

        if A is None:
            A = randint(0, 6)
        if B is None:
            B = randint(0, 6)
        self.A = A
        self.B = B
        if (self.A not in range(7)) or (self.B not in range(7)):
            raise AttributeError('Valeur incorrecte pour le domino')

    def score(self):
        """
        Calcul des points du domino
        """
        return self.A + self.B

    def inverse(self):
        """
        Inverse la position du domino
        """
        self.A, self.B = self.B, self.A
        return self

    def __repr__(self):
        """
        Renvoie une rpésentation de l'objet
        """
        if self.A == 0:
            return f"[ :{self.B}]"
        elif self.B == 0:
            return f"[{self.A}:]"
        elif self.A == 0 and self.B == 0:
            return f"[ : ]"
        else:
            return f"[{self.A}:{self.B}]"

    def est_compatible(self, valeur):
        """
        Teste si le domino est compatible avec une valeur passée en paramètre
        c'est-à-dire s'il peut être placée à côté de cette valeur
        """
        if valeur == 0:
            return True
        if self.A == valeur or self.B == valeur or self.A == 0 or self.B == 0:
            return True
        else:
            return False

    def peut_etre_place_a_gauche_de(self, valeur):
        if valeur == 0:
            return True
        if self.B == valeur or self.B == 0:
            return True
        else:
            return False

    def peut_etre_place_a_droite_de(self, valeur):
        if valeur == 0:
            return True
        if self.A == valeur or self.A == 0:
            return True
        else:
            return False


class Talon:

    """
    Talon (pioche) du jeu de domino
    """

    def __init__(self, t=None):

        # Liste defini
        if t == None:
            dominos = [0, 1, 2, 3, 4, 5, 6]
            dominos2 = [0, 1, 2, 3, 4, 5, 6]
            liste_dominos = []
            liste_dominos_talons = []

            for x in dominos:
                for y in dominos2:

                    d1 = Domino(x, y)
                    liste_dominos.append(Domino(x, y))
                dominos2.pop(0)

            # liste de 10

            for x in range(10):

                dominos_talons = choice(liste_dominos)
                liste_dominos_talons.append(dominos_talons)
                liste_dominos.remove(dominos_talons)

            self.t = liste_dominos_talons

        else:
            self.t = t
            self.vide = False

    def pioche(self):

        if len(self.t) == 1:
            self.t = []
        elif len(self.t) == 0:
            return None
        else:
            domino_pioche = []
            domino_random = choice(self.t)
            domino_pioche.append(domino_random)
            self.t = domino_pioche

    def __repr__(self):
        return f"[{self.t}:{self.t}]"

    @classmethod
    def de_taille(cls, n):  # from_integer(cls, n):
        talon = cls([Domino() for _ in range(n)])
        return talon


class Chaine:
    """
    Représentation de la chaîne de jeu
    exemple: [1: ][3:5][5:2]
    En plus de la liste ordonnée des dominos selon le placement,
    Deux valeurs seront stockées:
    A dans notre exemple 1
    B dans notre exemple 2
    """
    c = []

    def __init__(self):
        self.c = []
        self.A = None
        self.B = None

    def extremites(self):

        print("chaine : "+str(self.c))

        self.A = self.c[0].A
        self.B = self.c[-1].B

    def pose_premier(self, domino):

        self.A = domino.A
        self.B = domino.B

        self.c.append(domino)
        print("chaine : "+str(self.c))

    def ajoute_au_cote_gauche(self, domino):

        self.c.insert(0, domino)

        self.A = domino.A

        self.extremites()

    def ajoute_au_cote_droite(self, domino):

        self.c.insert(len(self.c), domino)
        self.extremites()

    def __repr__(self):

        return str(self.c)


class Main:
    """
    Gestion de la main du joueur
    m : liste de dominos
    """

    def __init__(self, m=None):
        self.m = m

        if m == None:
            liste_dominos = []

            for i in range(0, 7):
                for j in range(i, 7):
                    liste_dominos.append(Domino(i, j))

            dominos_main = sample(liste_dominos, k=5)
            self.m = dominos_main

        else:
            self.m = m

    def plus_haute_valeur(self):
        # Plus haute valeur dans la main du joueur
        score = None
        position = None

        for key, val in enumerate(self.m):
            score = Domino.score(val)
        position = key
        return (score, position)

    def domino_de_plus_haute_valeur(self):
        # Extrait et renvoie le domino de plus haute valeur
        _, position_max = self.plus_haute_valeur()
        domino_choisi = None

        if position_max is not None:
            domino_choisi = self.m.pop(position_max)
        return domino_choisi

    def selectionne(self, indice):
        # Selectionne le domino tout en le laissant dans la main
        domino_choisi = self.m[indice]
        return domino_choisi

    def retire(self, indice):
        # Retire le domino de la main
        domino_choisi = self.m.pop(indice)
        return domino_choisi

    def est_compatible(self, valeur):
        # Renvoie vrai s'il y a un domino dans la main compatible avec la valeur passée en paramètre
        compatible = False

        for i in self.m:
            compatible = Domino.est_compatible(i, valeur)

        return compatible

    def ajoute(self, domino):
        self.m.append(domino)

    def __repr__(self):
        representation = ""
        if self.m:
            for domino in self.m:
                representation += repr(domino)
        return representation

    @classmethod
    def de_taille(cls, n):
        main = cls([Domino() for _ in range(n)])
        return main


class Joueur:
    def __init__(self, main=None):
        if main == None:
            self.main = Main()
        else:
            self.main = main
        self.chaine = None  # vue sur la chaîne

    def regarde_le_plateau(self, chaine):
        self.chaine = chaine

    def peut_jouer(self):
        if self.main.est_compatible(self.chaine.B) or self.main.est_compatible(self.chaine.A):
            return True
        else:
            return False

    def choisit(self):
        raise NotImplementedError()

    def revendique_la_victoire(self):
        if len(self.main.m) == 0:
            return True
        else:
            return False


class Humain(Joueur):

    def __init__(self, main=None):
        self.type_de_joueur = 'HUMAIN'
        super().__init__(main)

    def affiche_main(self):
        # Affichage
        indices = ''
        for i in range(len(self.main.m)):
            indices += f'  {i+1}  '
        print(self.main, 'HUMAIN')
        print(indices)

    def choisit(self):
        position_impossible = True
        while position_impossible:
            # Affichage
            indices = ''
            for i in range(len(self.main.m)):
                indices += f'  {i+1}  '
            print(self.main, 'HUMAIN')
            print(indices)

            # Sélection
            position = 0
            while position not in range(1, len(self.main.m) + 1):
                position = int(input("Selectionner le "))
            domino = self.main.selectionne(position - 1)
            print(domino)

            # Inversion
            inversion = ''
            while inversion not in ['o', 'O', 'n', 'N']:
                inversion = input('Inverser [o/n]: ')
            if inversion in ['o', 'O']:
                domino.inverse()
                print(domino)

            # Placement avec test de validité du positionnement
            placement = ''
            while placement not in ['g', 'G', 'd', 'D']:
                placement = input(
                    'Placer à gauche (g) ou à droite (d) [g/d]: ')
            if placement in ['g', 'G']:
                placement = 'gauche'
                if domino.peut_etre_place_a_gauche_de(self.chaine.A):
                    position_impossible = False
                    self.main.retire(position - 1)
                else:
                    print('Positionnement impossible')
                    if inversion in ['o', 'O']:
                        domino.inverse()
            else:
                placement = 'droite'
                if domino.peut_etre_place_a_droite_de(self.chaine.B):
                    position_impossible = False
                    self.main.retire(position - 1)
                else:
                    print('Positionnement impossible')
                    if inversion in ['o', 'O']:
                        domino.inverse()

        return domino, placement
